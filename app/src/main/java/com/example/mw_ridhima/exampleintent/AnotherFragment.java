package com.example.mw_ridhima.exampleintent;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by MW_Ridhima on 8/30/2017.
 */

public class AnotherFragment extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.another_fragment);
        Intent intent=getIntent();
      int ind=  intent.getIntExtra("index",0);
        FragmentB fragmentB=(FragmentB) getFragmentManager().findFragmentById(R.id.fragmentB);
        if(fragmentB!=null && fragmentB.isVisible()) {
            fragmentB.changeData(ind);
        }
    }
}
