package com.example.mw_ridhima.exampleintent;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by MW_Ridhima on 8/28/2017.
 */


public class FragmentA extends Fragment implements AdapterView.OnItemClickListener{
ListView listt_frg;
Comm com;
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_a,container,false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listt_frg= (ListView) getActivity().findViewById(R.id.listt_frg);
        ArrayAdapter adapter=ArrayAdapter.createFromResource(getActivity(),R.array.name,R.layout.support_simple_spinner_dropdown_item);
        listt_frg.setAdapter(adapter);
        listt_frg.setOnItemClickListener(this);
    }

    public void setCom(Comm com) {
        this.com = com;
    }

    public interface Comm{
public void respond(int index);
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
com.respond(i);
    }
}
