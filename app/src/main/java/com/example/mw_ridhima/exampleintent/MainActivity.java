package com.example.mw_ridhima.exampleintent;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity implements FragmentA.Comm {
    FragmentA fragmentA;
    FragmentB fragmentB;
    FragmentManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        for(int i=1;i<10;i++){
            /*do something*/
        }
        manager=getFragmentManager();
        fragmentA=(FragmentA) manager.findFragmentById(R.id.fragmentA);
        fragmentA.setCom(this);
    }


    @Override
    public void respond(int index) {
fragmentB=(FragmentB) manager.findFragmentById(R.id.fragmentB);
        if(fragmentB!=null && fragmentB.isVisible()){
            fragmentB.changeData(index);
        }else
        {
            Intent intent=new Intent(this,AnotherFragment.class);
            intent.putExtra("index",index);
            startActivity(intent);
        }
    }
    public void add(View view){

    }
    public void remove(View view){}

}
